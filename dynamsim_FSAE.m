%     Copyright (c) 2016 David Zimmerman
% 
% 
%     Permission is hereby granted, free of charge, to any person obtaining
%     a copy of this software and associated documentation files (the
%     "Software"), to deal in the Software without restriction, including
%     without limitation the rights to use, copy, modify, merge, publish,
%     distribute, sublicense, and/or sell copies of the Software, and to
%     permit persons to whom the Software is furnished to do so, subject to
%     the following conditions:
% 
%     The above copyright notice and this permission notice shall be
%     included in all copies or substantial portions of the Software.
% 
%     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
%     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
%     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
%     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
%     BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
%     ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
%     CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%     SOFTWARE.
%% 


function [t1, xa] = dynamsim_FSAE()
global Izz frontRate rearRate sRear sFront d m a b par0 aerodist cl FA

%Main tuning coefficients:
%weight distribution controls F:R weight relative to wheelbase
wDist=.5;

%TLLTD controls F:R weight transfer characteristics
TLLTD=.5;

%Aerodist controls F:R aero downforce distribution
aerodist=.3;

%Lateral Load transfer per G, controlled by cog height and track width
LTG=183; %lb

wheelBase=60*.0254; %meters
a=wheelBase*wDist;
b=wheelBase*(1-wDist);
m = 260; %Vehicle and Driver mass, kg
Izz = 150; %Yaw Inertia about vertical axis kg-m^2
cl=2; %Coefficient of Lift
FA=1.36; %Frontal Area of car, m^2
transferRate=(LTG)*4.44;%N/g, transfer rate taken from spreadsheet calculations

d=5; %Degrees. Conversions happen later.
tStart=0;
tEnd=10;
%a=30*.0254; %meters
%b=30*.0254; %meters
%wDist=a/(b+a);

%d1=5;   %In case of implementing step or sine wave

%frontRC = 1*.0252; %m
%rearRC = 2*.0252; %m
%cgHeight = 13*.0252; %m 
%wheelBase=60*.0252; %m

%load('Hoosier Cy matrix.mat')

%Calculate weight transfer rate
frontRate=transferRate*TLLTD; %N/g
rearRate=transferRate*(1-TLLTD); %N/g

%Static front and rear weights
sFront=m*wDist;
sRear=m*(1-wDist);

load('pacejka5sol.mat');
%% 
for i=1:40
opts = odeset('RelTol',1.e-5);
[t1 xa] = ode45(@slip,[tStart tEnd], [i, 0, 0], opts);

Vx(i)=xa(end,1);
Vy(i)=xa(end,2);
r(i)=xa(end,3);

if(i==15)
    figure(1)
   plot(t1(:),(xa(:,1).*xa(:,3))/(9.81)); 
   title('Acceleration response to steering input')
   ylabel('Lateral Acceleration, Ay (Gs)')
   xlabel('Time (s)')
   
end
end
%% 
Ay=Vx.*r./9.81;
R=Vx./r;
Sk=(1./(R.*deg2rad(d)));
Sa=Ay./(deg2rad(d).*9.81);
%% 
%Plot curvature response
figure(2);
plot(Vx,Sk)
   title('Curvature Response of Vehicle')
   ylabel('Curvature Response  -  1/(delta*R)')
   xlabel('Longitudinal Velocity - (m/s)')
   axis([0 40 .5 .75]);
figure(3);
   plot(Vx,Sa)
   title('Acceleration Response of Vehicle')
   ylabel('Acceleration Response  -  Ay/(delta*g)')
   xlabel('Longitudinal Velocity - (m/s)')
   axis([0 40 0 15]);
%figure(1);
%plot(t1,Ay);
end

function s=slip(t, x)
global a b Izz frontRate rearRate sRear sFront d m par0 aerodist cl FA

Vx=x(1);
Vy=x(2);
r=x(3);
%%
%Implement step steer maneuver
%if(t>=.1)
%   d=d1.*sawtooth((t-.1).*5); 
%end

%Load curve fit
%load('Hoosier 10in 75 tire 8 rim.mat');

%beta=x(3);
%Constant longitudinal Velocity

%Calculate Front and rear tire slip angles
%Jazar eq. 10.133
%Tire Sideslip angle taking into account yaw and body slip angle
%It's negative from the steering angle, and this boggles my mind.
%alphf=beta+a*r/Vx-deg2rad(d);
%Jazar eq. 10.134
%alphr=beta-b*r/Vx;

%Calculates cornering stiffness at the slip angle and force of each tire
%This is an incredibly slow way to get these numbers.
%If speed is an issue, find another way
%[Calphfl]=griddata(Fzc,SAc,Cy,Fz(1),rad2deg(alphf))*180/pi();
%[Calphfr]=griddata(Fzc,SAc,Cy,Fz(2),rad2deg(alphf))*180/pi();
%[Calphrl]=griddata(Fzc,SAc,Cy,Fz(3),rad2deg(alphr))*180/pi();
%[Calphrr]=griddata(Fzc,SAc,Cy,Fz(4),rad2deg(alphr))*180/pi();

%A slower way of doing things:
%[Calphfl]=differentiate(fittedmodel, [rad2deg(alphf),Fz(2)])*180/pi();
%[Calphfr]=differentiate(fittedmodel, [rad2deg(alphf),Fz(2)])*180/pi();
%[Calphrl]=differentiate(fittedmodel, [rad2deg(alphr),Fz(3)])*180/pi();
%[Calphrr]=differentiate(fittedmodel, [rad2deg(alphr),Fz(4)])*180/pi();

%Calculate axle stiffness
%Calphf=20000;%-(Calphfr+Calphfl);
%Calphr=20000;%-(Calphrl+Calphrr);

%Jazar eq. 10.501-10.505
%Vx=constant, Vxdot=0;

%vyDot = r./(m.*Vx).*(-a.*Calphf+b.*Calphr)-Vy./(m.*Vx).*(Calphf+Calphr) + 1./m.*Calphf.*deg2rad(d); % + 1/m.*Calphr*alphr;
%rDot = r./(Izz.*Vx)*(-a^2.*Calphf-b^2.*Calphr) - Vy./(Izz.*Vx).*(a.*Calphf-b.*Calphr)+a.*Calphf.*deg2rad(d)./Izz;%-b*Calphr.*alphr./Izz;
%betaDot = -(Calphf + Calphr)./(m.*Vx).*beta+ ((-a.*Calphf+b.*Calphr)./(m*Vx^2)-1).*r + 1./m.*Calphf.*deg2rad(d);%+1./m.*Calphr.*alphr;
%% 

Ay=Vx.*r;

downF=.5.*cl.*FA.*1.20.*Vx.^2;
f_D= (downF * aerodist)/2;
r_D= (downF * (1-aerodist))/2;


Fz(1)= sFront * 9.81/2 + Ay * frontRate /(9.81*2) + f_D;
Fz(2)= sFront * 9.81/2 - Ay * frontRate /(9.81*2) + f_D;
Fz(3)= sRear  * 9.81/2 + Ay * rearRate  /(9.81*2) + r_D;
Fz(4)= sRear  * 9.81/2 - Ay * rearRate  /(9.81*2) + r_D;

%Pacejka model force based method:
frontSlip=rad2deg(-a.*r./Vx-Vy./Vx+deg2rad(d));
rearSlip=rad2deg(b.*r./Vx-Vy./Vx);

Frfl=-Pacejka5Fy([frontSlip,Fz(1),0,96.4],par0);
Frfr=-Pacejka5Fy([frontSlip,Fz(2),0,96.4],par0);
Frrl=-Pacejka5Fy([rearSlip,Fz(3),0,96.4],par0);
Frrr=-Pacejka5Fy([rearSlip,Fz(4),0,96.4],par0);

Fx=-r.*Vy.*m;%r.*Vy.*m;

vxDot=Fx/m+r.*Vy;
vyDot=(Frfl+Frfr+Frrl+Frrr)./m;
rDot=(a.*(Frfl+Frfr)-b.*(Frrl+Frrr))./Izz;

%Calculate new vertical load from load transfer given acceleration

s = [vxDot; vyDot; rDot];

end
      