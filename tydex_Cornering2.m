% This file writes TYDEX formatted files for use in the Delft Tyre software
% suite. You should be able to simply run the file, select a round 6 or
% round 7 cornering run that you want to clean up and format the data for,
% and this program will place the necessary files under a tydex directory
% that you run this program from.
%% 

%     Copyright (c) 2016 David Zimmerman
% 
% 
%     Permission is hereby granted, free of charge, to any person obtaining
%     a copy of this software and associated documentation files (the
%     "Software"), to deal in the Software without restriction, including
%     without limitation the rights to use, copy, modify, merge, publish,
%     distribute, sublicense, and/or sell copies of the Software, and to
%     permit persons to whom the Software is furnished to do so, subject to
%     the following conditions:
% 
%     The above copyright notice and this permission notice shall be
%     included in all copies or substantial portions of the Software.
% 
%     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
%     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
%     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
%     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
%     BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
%     ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
%     CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%     SOFTWARE.

%%
%MODIFY TO MATCH TEST CONDITIONS
clc
clear

TYDEXFMT = 1.3;
LONGSLIP=0;%-
FZ_inc=217;%N
P_inc=13.78;%kPa
IA_inc=2;%deg
V_inc=16.09;%kph
%%
DTE = datetime('today');
%NOMWIDTH = input('Nominal section width of tire in meters: ');
%RIMDIAME = input('Input Rim Diameter in meters: ');
%RIMWIDTH = input('Input Rim Width in meters: ');

[filename,pathname]= uigetfile('*.mat','Enter TIRF Test File','C:\Files\Data\MATLAB');
t=([pathname filename]);

load(sprintf('%s%s',pathname,filename));
C=textscan(tireid(1,:),'%s');


%Co-ordinate transformation: SAE to ISO
    %MF-Tool expects data to be in ISO. Of course, the TTC data
    %is output in SAE coordinates. This is a conversion attempt. 
SA=-SA;
%FX=FX;
FY=-FY;
FZ=-FZ;
%MX=MX;
MZ=-MZ;
%IA=IA;
%SR=SR;


SAc=[];
FZc=[];
FYc=[];
MXc=[];
IAc=[];
MZc=[];
Pc=[];
Cc=[];
%Try to extract characteristics of tire from file name. Continental,
%Hoosier 13 and hoosier 10 inch tires are all formatted differently. This
%has really only been tested well for the 10 inch R25B tires.


try
    RIMDIAME = cellfun(@str2num,(C{1}(5)))*.0254;
catch
    RIMDIAME = 13*.0254;
end

destPath=sprintf('%s%s\\%s\\%s',pathname,tireid,testid);
try
    RIMWIDTH = cellfun(@str2num,(C{1}(9)))*.0254;
catch
    try
        RIMWIDTH = cellfun(@str2num,(C{1}(10)))*.0254;
    catch
        try
           RIMWIDTH = cellfun(@str2num,(C{1}(4)))*.0254;
        catch
           RIMWIDTH = cellfun(@str2num,(C{1}(3)))*.0254;
        end
    end
end
try
    NOMWIDTH = cellfun(@str2num,(C{1}(4)))*.0254;
catch
    NOMWIDTH = (C{1,1}{2,1}(1:3));
end
MANUFACT = C{1}{1};

chname=getfield(channel,'name');
chunit=getfield(channel,'units');
increments=max(FZ)/FZ_inc;

%Split up into bins
FZ_bins=FZ_inc:FZ_inc:FZ_inc*7;
P_bins=57:P_inc:(57+P_inc*3);
IA_bins=0:IA_inc:IA_inc*2;
V_bins=[24 40 72];

for i=1:length(FZ_bins)
stuff=find(FZ<(FZ_bins(i)+FZ_inc/2) & FZ>(FZ_bins(i)-FZ_inc/2));
FZloc{i}=stuff;
end

for j=1:length(P_bins)
stuff1=find(P<(P_bins(j)+P_inc/2) & P>(P_bins(j)-P_inc/2));
Ploc{j}=stuff1;
end

for k=1:length(IA_bins)
stuff2=find(IA<(IA_bins(k)+IA_inc/2) & IA>(IA_bins(k)-IA_inc/2));
IAloc{k}=stuff2;
end

for l=1:length(V_bins)
stuff3=find(V<(V_bins(l)+V_inc/2) & V>(V_bins(l)-V_inc/2));
Vloc{l}=stuff3;
end

figure('units','normalized','outerposition',[0 0 1 1])

for i=1:length(FZ_bins)
    for j=1:length(P_bins)
        for k=1:length(IA_bins)
            for l=1:length(V_bins)
                common{i,j,k,l}=intersect(Vloc{l},intersect(IAloc{k},intersect(Ploc{j},FZloc{i})));
            end
        end
    end
end

for i=1:length(FZ_bins)
    for j=1:length(P_bins)
        for k=1:length(IA_bins)
            for l=1:length(V_bins)
                if ~common{i,j,k,l}
                    break
                end
                FZav  = round(mean( FZ(common{i,j,k,l}))/FZ_inc)*FZ_inc;
                if isnan(FZav)
                    continue
                end
                if (length(common{i,j,k,l}) < 101)
                    continue
                end
                
                Pav   = round(mean( P(common{i,j,k,l}))/P_inc)*P_inc;
                IAav  = round(mean( IA(common{i,j,k,l}))/IA_inc)*IA_inc;
                Vav   = round(mean( V(common{i,j,k,l}))/V_inc)*V_inc;
                A=[FZav Pav IAav Vav];
                disp(A);

                
                newFile=sprintf('%s_%4.0f_%3.0f_%2.0f_%2.0f.tdx',filename,FZav,Pav,IAav,Vav);
                tDir=fullfile([pwd '\\' 'Tydex' '\\' strrep(tireid,'/','_') '\\' strrep(testid,'/','_')]);
                
                if ~exist(tDir, 'dir')
                  mkdir(tDir);
                end

                [fid,errmsg]=fopen(fullfile(tDir,newFile),'wt');
                
                fprintf(fid,'Column\n');
                fprintf(fid,'1         11                            41        51        61        71\n');

                fprintf(fid,'\n**HEADER\n');
                fprintf(fid,'RELEASE   Release of TYDEX-Format                 %3.1f\n',TYDEXFMT);
                fprintf(fid,'MANUFACT  Manufacturer                            %s\n',MANUFACT);
                fprintf(fid,'MEASID    Measurement id                          %s\n',testid);
                fprintf(fid,'SUPPLIER  Data supplier                           TTC\n');
                fprintf(fid,'DATE      Date                                    %s\n',DTE);

                fprintf(fid,'\n**CONSTANTS\n');

                %fprintf(fid,'IDENTITY  Identity                                %3.1f\n',tireid);
                fprintf(fid,'INFLPRES  Infl. Pres (tyre at amb.temp) kPa       %-6.3f\n',Pav);
                fprintf(fid,'RIMWIDTH  Rim width                     m         %-6.3f\n',RIMWIDTH);
                fprintf(fid,'RIMDIAME  Nominal rim diameter          m         %-6.3f\n',RIMDIAME);
                %fprintf(fid,'ASPRATIO  Nominal aspect ratio          %
                %%3.1f\n',ASPRATIO);
                fprintf(fid,'NOMWIDTH  Nominal section width of tyre m         %-6.3f\n',NOMWIDTH);
                fprintf(fid,'FZW       Nominal wheel load            N         %-5.0f\n',FZav);
                fprintf(fid,'TRAJVELW  Trajectory velocity           km/h      %3.2f\n',Vav);
                fprintf(fid,'INCLANGL  Nominal camber angle          deg       %1.0f\n',IAav);
                fprintf(fid,'LONGSLIP  Longitudinal slip             -         0\n');

                fprintf(fid,'\n**MEASURCHANNELS\n');

                fprintf(fid,'MEASNUMB  Measurement Point No.         -         1         0         0\n');
                fprintf(fid,'SLIPANGL  Slip angle        (Alpha)     deg       1         0         0\n');
                %fprintf(fid,'INCLANGL  Camber angle      (Gamma)     rad       1         0         0\n');
                %fprintf(fid,'LONGSLIP  Longitidinal slip (Kappa)     -         1         0         0\n');
                %fprintf(fid,'FX        Longitudinal force   (Fx)     N         1         0         0\n');
                fprintf(fid,'FYW       Side force           (Fy)     N         1         0         0\n');
                fprintf(fid,'FZW       Wheel load           (Fz)     N         1         0         0\n');
                fprintf(fid,'MXW       Overturning couple   (Mx)     Nm        1         0         0\n');
                fprintf(fid,'MZW       Aligning Torque      (Mz)     Nm        1         0         0\n');
                fprintf(fid,'\n**MEASURDATA\n');
                
                SAd{i,j,k,l}=decimate(SA(common{i,j,k,l}),10);
                %SRd{i,j,k,l}=decimate(SR(common{i,j,k,l}),10);
                
                MXd{i,j,k,l}=decimate(smooth(SA(common{i,j,k,l}),MX(common{i,j,k,l}),301,'rlowess'),10);
                subplot(2,3,1)
                plot(SA(common{i,j,k,l}),MX(common{i,j,k,l}));
                hold on
                plot(interp(SAd{i,j,k,l},10),interp(MXd{i,j,k,l},10));
                hold off
                title('SA v MX');
                xlabel(['Inclination Angle: ' num2str(IAav) 'Deg'])
                drawnow update
                
                MZd{i,j,k,l}=decimate(smooth(SA(common{i,j,k,l}),MZ(common{i,j,k,l}),201,'rlowess'),10);
                subplot(2,3,2)
                plot(SA(common{i,j,k,l}),MZ(common{i,j,k,l}));
                hold on
                plot(interp(SAd{i,j,k,l},10),interp(MZd{i,j,k,l},10));
                hold off
                title('SA v MZ');
                drawnow update
                
%                 FXd{i,j,k,l}=decimate(smooth(SA(common{i,j,k,l}),FX(common{i,j,k,l}),71,'rlowess'),10);
%                 subplot(5,1,3)
%                 plot(SA(common{i,j,k,l}),FX(common{i,j,k,l}));
%                 hold on
%                 plot(interp(SAd{i,j,k,l},10),interp(FXd{i,j,k,l},10));
%                 hold off
%                 title('SA v FX');
%                 drawnow update
                
                FYd{i,j,k,l}=decimate(smooth(SA(common{i,j,k,l}),FY(common{i,j,k,l}),101,'rlowess'),10);
                subplot(5,1,4)
                plot(SA(common{i,j,k,l}),FY(common{i,j,k,l}));
                hold on
                plot(interp(SAd{i,j,k,l},10),interp(FYd{i,j,k,l},10));
                hold off
                title('SA v FY');
                
                drawnow update
                
                FZd{i,j,k,l}=decimate(smooth(SA(common{i,j,k,l}),FZ(common{i,j,k,l}),301,'rlowess'),10);
                subplot(5,1,5)
                plot(SA(common{i,j,k,l}),FZ(common{i,j,k,l}));
                hold on
                plot(interp(SAd{i,j,k,l},10),interp(FZd{i,j,k,l},10));
                hold off
                title('SA v FZ');
                drawnow update

                
                for n=1:length(SAd{i,j,k,l})
                    %Converts data from SAE to W-coordinate system
                    fprintf(fid,'% 10i% 10.5f% 10.3f% 10.3f% 10.3f% 10.3f\n', ...
                        n, ...
                        SAd{i,j,k,l}(n), ...
                        FYd{i,j,k,l}(n), ...
                        FZd{i,j,k,l}(n), ...
                        MXd{i,j,k,l}(n), ...
                        MZd{i,j,k,l}(n));
                end
                fprintf(fid,'\n**MODELPARAMETERS 2\n');

                fprintf(fid,'FZ_NOM    Nominal Vertical Load         N         651\n');
                fprintf(fid,'NOMPRES   Nominal Inflation Pressure    kPa       82.68\n');

                fprintf(fid,'**END');
                fclose(fid);
                
                if ((l==2))
                    %This section outputs for if you want to keep the
                    %cleaned up curves. May need some attention with
                    %regards to the naming.
                    SAc =cat(1,SAc,SAd{i,j,k,l}(:));
                    FZc =cat(1,FZc,FZd{i,j,k,l}(:));
                    FYc =cat(1,FYc,FYd{i,j,k,l}(:));
                    MXc =cat(1,MXc,MXd{i,j,k,l}(:));
                    MZc =cat(1,MZc,MZd{i,j,k,l}(:));
                    IAc =cat(1,IAc,IAav.*ones(length(SAd{i,j,k,l}(:)),1));
                    Pc  =cat(1,Pc,Pav.*ones(length(SAd{i,j,k,l}(:)),1));
                    %Cc  =cat(1,Cc,diff(FYd{i,j,k,l},1,SAd{i,j,k,l}));
                    save('Cleaned Hoosier 10 inch 7.5 tire 8 rim R25B','FYc','SAc','FZc','IAc','Pc','MZc','MXc')
                end
            end
        end
    end
end

%Plot to visualise data:
figure(2)
subplot(2,3,1)
hold on
for i=1:6
    try
       plot(SAd{i,2,1,2},FYd{i,2,1,2}) 
       plot(SA(common{i,2,1,2}),FY(common{i,2,1,2}),'linestyle','--')
       title('FZ influence on FY');
    catch
    end
end
subplot(2,3,2)
hold on
for i=1:3
    plot(SAd{2,2,i,2},FYd{2,2,i,2})
    plot(SA(common{2,2,i,2}),FY(common{2,2,i,2}))
    title('Camber influence on Fy')
end


subplot(2,3,3)
hold on
for i=1:6
    try
        plot(SAd{i,2,1,2},MZd{i,2,1,2})
        plot(SA(common{i,2,1,2}),MZ(common{i,2,1,2}))
        title('FZ influence on MZ');
    catch
    end
end

subplot(2,3,4)
hold on
for i=1:6
    try
        plot(SAd{2,2,i,2},MXd{2,2,i,2}) 
        plot(SA(common{2,2,i,2}),MX(common{2,2,i,2}))
        title('Camber influence on MX');
    catch
    end
end

subplot(2,3,5)
hold on
for i=1:6
    try
        plot(SAd{i,2,1,2},MXd{i,2,1,2})
        plot(SA(common{i,2,1,2}),MX(common{i,2,1,2}))
        title('FZ influence on MX');
    catch
    end
end

subplot(2,3,6)
hold on
for i=1:6
    try
        plot(SAd{2,2,i,2},MZd{2,2,i,2}) 
        plot(SA(common{2,2,i,2}),MZ(common{2,2,i,2}))
        title('Camber influence on MZ');
    catch
    end
end
