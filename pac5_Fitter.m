%This program is intended to fit FSAE TTC Tire data to the Pacejka
%equations. Please note that the license which follows does not apply to
%the data itself, nor may it apply to curves resulting from the data.
%Please be aware of the licensing terms for the data you are using.


%     Copyright (c) 2016 David Zimmerman
% 
% 
%     Permission is hereby granted, free of charge, to any person obtaining
%     a copy of this software and associated documentation files (the
%     "Software"), to deal in the Software without restriction, including
%     without limitation the rights to use, copy, modify, merge, publish,
%     distribute, sublicense, and/or sell copies of the Software, and to
%     permit persons to whom the Software is furnished to do so, subject to
%     the following conditions:
% 
%     The above copyright notice and this permission notice shall be
%     included in all copies or substantial portions of the Software.
% 
%     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
%     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
%     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
%     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
%     BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
%     ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
%     CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%     SOFTWARE.

%load('pacejka4base.mat');
load('Cleaned Hoosier 10 inch 75 tire 8 rim R25B.mat')
xdata2=[SAc, FZc, IAc, Pc];
ydata2=FYc;

fun=@(a,xdata) Pacejka5Fy(xdata,a);
opts = statset('nlinfit');
opts.RobustWgtFun = 'andrews';
opts.MaxIter=00;
opts.TolX=1e-13;

par0=nlinfit(xdata2,ydata2,fun,par0,opts);
save('pacejka5sol2','par0')

SAn=linspace(-15,15,200)';

hold off
figure(1)
plot3(SAc,FZc,FYc);
hold on;
fyS=Pacejka5Fy(xdata2, par0);
plot3(SAc,FZc,fyS);
