%This Function provides an implementation of Pacejka's 2004 equations
%modified for taking tire pressure inputs. Have tested it with FSAE tire
%data courtesy of the TTC, and it provided decent fits. Watch out for
%coordinate trickery, My purposes seem to implement a mishmash of SAE and
%ISO Tire coordinate system definitions.
%
%%
%     Copyright (c) 2016 David Zimmerman
% 
% 
%     Permission is hereby granted, free of charge, to any person obtaining
%     a copy of this software and associated documentation files (the
%     "Software"), to deal in the Software without restriction, including
%     without limitation the rights to use, copy, modify, merge, publish,
%     distribute, sublicense, and/or sell copies of the Software, and to
%     permit persons to whom the Software is furnished to do so, subject to
%     the following conditions:
% 
%     The above copyright notice and this permission notice shall be
%     included in all copies or substantial portions of the Software.
% 
%     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
%     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
%     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
%     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
%     BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
%     ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
%     CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%     SOFTWARE.


function Fy = Pacejka5Fy(xdata, a)

SA = xdata(:,1);
Fz = xdata(:,2);
IA = xdata(:,3);
P  = xdata(:,4);

mu=.6;
FZ0=880;
P0=96.4;

dfz=(Fz-FZ0)./FZ0;
dp=(P-P0)./P0;


%Fz=Fz;
SA=deg2rad(SA);
IA=deg2rad(IA);
C=a(1);
    
    
    D=((a(2)+a(3).*dfz).*(1+a(23).*dp+a(24).*dp.^2)./(1+a(4).*IA.^2)).*mu.*Fz;
    E=(a(5)+a(6).*dfz).*(1+a(7).*IA.^2-(a(8)+a(9).*IA).*sign(SA));
    BCD=a(10).*FZ0.*(1+a(27).*dp).*sin(a(11).*atan(Fz./((1+a(26).*dp).*(a(12)+a(13).*IA.^2).*FZ0)))./(1+a(14).*IA.^2);
    B=BCD./(C.*D);
    
    KYG=Fz.*(a(15)+a(16).*dfz).*(1+a(25).*dp);
    
    Svyg=Fz.*(a(17)+a(18).*dfz).*IA;
    Svy=Fz.*(a(19)+a(20).*dfz)+Svyg;
    Shy=(a(21)+a(22).*dfz)+(KYG.*IA-Svyg)./(BCD);
    SA1=SA+Shy;
    
    Fy = -(D .* sin(C .* atan(B.*SA1-E.*(B.*SA1-atan(B.*SA1))))+Svy);
    
end